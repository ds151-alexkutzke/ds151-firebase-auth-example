import { useState } from 'react';
import { StyleSheet, View } from 'react-native';
import { Text, Input, Button } from "@rneui/base";
import {apiKey,authDomain,projectId,storageBucket,messagingSenderId,appId} from "@env"
import { initializeApp } from 'firebase/app';
import { getAuth, connectAuthEmulator, createUserWithEmailAndPassword } from "firebase/auth";

const firebaseConfig = {
  apiKey, authDomain,
  projectId,
  storageBucket,
  messagingSenderId,
  appId
}

// Initialize Firebase
const app = initializeApp(firebaseConfig);

// Initialize Firebase Authentication and get a reference to the service
const auth = getAuth(app);
connectAuthEmulator(auth, "http://10.0.2.2:9099");

export default function App() {
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");

  return (
    <View style={styles.container}>
      <Input
        placeholder="Email"
        onChangeText={(value) => setEmail(value)}
        value={email}
      />
      <Input
        placeholder="Password"
        onChangeText={(value) => setPassword(value)}
        value={password}
        secureTextEntry={true}
      />
      <Button
        title="Entrar"
        onPress={async () => {
          createUserWithEmailAndPassword(auth, email, password)
            .then((userCredential) => {
              // Signed in
              const user = userCredential.user;
              console.log(user);
              // ...
            })
            .catch((error) => {
              const errorCode = error.code;
              const errorMessage = error.message;
              console.log(errorMessage);
              // ..
            });
          /*      try{
const userCredential = await createUserWithEmailAndPassword(auth, email, password);

            const user = userCredential.user;
            console.log(user);
          }
          catch(error) {
            const errorCode = error.code;
            const errorMessage = error.message;
            console.log(errorMessage);
          }
*/
        }}
      />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    marginTop: 20,
  },
});
