# DS151 - Exemplo Firebase Auth com Expo

Este projeto foi preparado seguindo os passos da documentação do Expo sobre Firebase:

https://docs.expo.dev/guides/using-firebase/

Crie seu projeto no firebase antes de iniciar o processo.

Realize o clone do projeto e siga os passos abaixo:

1. Adicione todas as credenciais do projeto do firebase em um arquivo `.env` da seguinte forma:

```
apiKey="..."
authDomain="..."
projectId="..."
storageBucket="..."
messagingSenderId="..."
appId="..."
```

Execute:

```
npm install -g firebase-tools
npm install
firebase login
firebase init
firebase emulators:start # se desejar utilizar o emulador
npx expo start -a
```

